/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/


/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "tdc.h"

uint8_t _TDC_spi_tx_buffer[TDC_BUFFER_SIZE];
uint8_t _TDC_spi_rx_buffer[TDC_BUFFER_SIZE];


void TDC_Init( void )
{   
    // Initialize the buffers to 0
    memset( _TDC_spi_tx_buffer, 0, TDC_BUFFER_SIZE );
    memset( _TDC_spi_rx_buffer, 0, TDC_BUFFER_SIZE );
    
    // Update the Venus Shift Registers with 0s
    // TDC_Update();
}


void TDC_Update( void )
{
    // Clear SPI RX Buffer 
    memset( _TDC_spi_rx_buffer, '\0', TDC_BUFFER_SIZE );
    //
    // SPI_TDC_Transmit( );
    // Need delay in order for data to transfer properly
    CyDelayUs( 100 );
    //
    SPI_TDC_TransmitReceive();
    CyDelayUs( 100 );
    // SPI_TDC_TransmitReceive32();
}

/* [] END OF FILE */
