/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef DMA_H
#define DMA_H

#include "project.h"
#include "global_defs.h"
#include "spi.h"
#include "tdc.h"


// Define functions
void DMA_Init( void );
void DMA_SPI_TDC_TX_Config( void );
void DMA_SPI_TDC_TX_Enable( void );
void DMA_SPI_TDC_RX_Config( void );
void DMA_SPI_TDC_RX_Enable( void );

#endif

/* [] END OF FILE */


