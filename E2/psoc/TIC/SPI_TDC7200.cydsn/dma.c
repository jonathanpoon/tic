/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "dma.h"

//
extern uint8_t _TDC_spi_tx_buffer[TDC_BUFFER_SIZE];
extern uint8_t _TDC_spi_rx_buffer[TDC_BUFFER_SIZE];
extern uint8_t _SPI_TDC_tx_intr_disable_mask_reg;
extern uint8_t _SPI_TDC_rx_intr_disable_mask_reg;
//
uint8_t _DMA_SPI_TDC_TX_channel, _DMA_SPI_TDC_RX_channel;
uint8_t _DMA_SPI_TDC_TX_td[2], _DMA_SPI_TDC_RX_td[2];


void DMA_Init( void )
{
    DMA_SPI_TDC_TX_Config( );
    DMA_SPI_TDC_RX_Config( );
}


/* ------------------------------------------------------------------------------------- */

void DMA_SPI_TDC_TX_Config()
{
    // Init DMA Channel, 1 byte bursts, each burst requires a request */ 
    _DMA_SPI_TDC_TX_channel = DMA_SPI_TDC_TX_DmaInitialize( DMA_SPI_TDC_TX_BYTES_PER_BURST, DMA_SPI_TDC_TX_REQUEST_PER_BURST, HI16(DMA_SPI_TDC_TX_SRC_BASE), HI16(DMA_SPI_TDC_TX_DST_BASE) );
    // Allocate TD
    _DMA_SPI_TDC_TX_td[0] = CyDmaTdAllocate();
    _DMA_SPI_TDC_TX_td[1] = CyDmaTdAllocate();
        // TD to transfer data from SRAM buffer to SPIM TX FIFO Buffer
    CyDmaTdSetConfiguration( _DMA_SPI_TDC_TX_td[0], TDC_BUFFER_SIZE, _DMA_SPI_TDC_TX_td[1], CY_DMA_TD_INC_SRC_ADR );
    // TD to disable the SPI Master TX interrupt
    CyDmaTdSetConfiguration( _DMA_SPI_TDC_TX_td[1], 1, DMA_DISABLE_TD, DMA_SPI_TDC_TX__TD_TERMOUT_EN );
    // From the memory to the SPIM
    CyDmaTdSetAddress( _DMA_SPI_TDC_TX_td[0], LO16((uint32)_TDC_spi_tx_buffer), LO16((uint32)SPI_TDC_TXDATA_PTR) );
    // From 'SPI_TDC_tx_intr_disable_mask_reg' to SPI_TDC_TX_STATUS_MASK_REG - 
    CyDmaTdSetAddress( _DMA_SPI_TDC_TX_td[1], LO16((uint32)&_SPI_TDC_tx_intr_disable_mask_reg), LO16((uint32)&SPI_TDC_TX_STATUS_MASK_REG));
}


void DMA_SPI_TDC_TX_Enable()
{
    // Associate the TD with the channel
    CyDmaChSetInitialTd( _DMA_SPI_TDC_TX_channel, _DMA_SPI_TDC_TX_td[0] );
    // Enable the SPI_TDC_TX_DMA_channel
    CyDmaChEnable( _DMA_SPI_TDC_TX_channel, ENABLE );
}


/* ------------------------------------------------------------------------------------- */

void DMA_SPI_TDC_RX_Config( )
{ 
    // Init DMA Channel, 1 byte bursts, each burst requires a request
    _DMA_SPI_TDC_RX_channel = DMA_SPI_TDC_RX_DmaInitialize( DMA_SPI_TDC_RX_BYTES_PER_BURST, DMA_SPI_TDC_RX_REQUEST_PER_BURST, HI16(DMA_SPI_TDC_RX_SRC_BASE), HI16(DMA_SPI_TDC_RX_DST_BASE) );
    // Allocate TD
    _DMA_SPI_TDC_RX_td[0] = CyDmaTdAllocate();
    _DMA_SPI_TDC_RX_td[1] = CyDmaTdAllocate();
    // TD to transfer data from SPIM RX FIFO Buffer to SRAM buffer
    CyDmaTdSetConfiguration( _DMA_SPI_TDC_RX_td[0], TDC_BUFFER_SIZE, _DMA_SPI_TDC_RX_td[1], CY_DMA_TD_INC_DST_ADR );
    // TD to disable the SPI Master RX interrupt
    CyDmaTdSetConfiguration( _DMA_SPI_TDC_RX_td[1], 1, DMA_DISABLE_TD, DMA_SPI_TDC_RX__TD_TERMOUT_EN );
    /* From the SPIM RX Buffer to SRAM */
    CyDmaTdSetAddress( _DMA_SPI_TDC_RX_td[0], LO16((uint32)SPI_TDC_RXDATA_PTR), LO16((uint32)_TDC_spi_rx_buffer) );
    // From 'SPI_TDC_rx_intr_disable_mask_reg' to SPI_TDC_RX_STATUS_MASK_REG - 
    CyDmaTdSetAddress( _DMA_SPI_TDC_RX_td[1], LO16((uint32)&_SPI_TDC_rx_intr_disable_mask_reg), LO16((uint32)&SPI_TDC_RX_STATUS_MASK_REG) );
}


void DMA_SPI_TDC_RX_Enable()
{
    // Associate the TD with the channel 
    CyDmaChSetInitialTd( _DMA_SPI_TDC_RX_channel, _DMA_SPI_TDC_RX_td[0] );
    // Enable the SPI_TDC_TX_DMA_channel
    CyDmaChEnable( _DMA_SPI_TDC_RX_channel, ENABLE );
}

/* [] END OF FILE */
