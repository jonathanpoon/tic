/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef TDC7200_H
#define TDC7200_H

#include "project.h"
#include "spi.h"

    
/* tdc.h Defines */
#define TDC7200_SPI_BUFFER_SIZE                             (4u)

// #define TDC7200_SPI_CLK_MAX                              (int32_t(20000000))
// #define TDC7200_SPI_MODE                                 (SPI_MODE0)
// #define TDC7200_SPI_ORDER                                (MSBFIRST)
#define TDC7200_SPI_REG_ADDR_MASK                           (0x3Fu)                 // (0x1Fu) previously
#define TDC7200_SPI_REG_READ                                (0x00u)
#define TDC7200_SPI_REG_WRITE                               (0x40u)
#define TDC7200_SPI_REG_AUTOINC                             (0x80u)
#define TDC7200_ENABLE_LOW_MS                               (5)
#define TDC7200_ENABLE_T3_LDO_SET3_MS                       (5)

#define TDC7200_RING_OSC_FREQ_HZ                            (144000000)
#define TDC7200_RING_OSC_FREQ_MHZ                           (TDC7200_RING_OSC_FREQ_HZ/1000000)
#define PS_PER_SEC                                          (1000000000000)
#define US_PER_SEC                                          (1000000)

#define TDC7200_REG_ADR_CONFIG1                             (0x00u)
#define TDC7200_REG_ADR_CONFIG2                             (0x01u)
#define TDC7200_REG_ADR_INT_STATUS                          (0x02u)
#define TDC7200_REG_ADR_INT_MASK                            (0x03u)
#define TDC7200_REG_ADR_COARSE_CNTR_OVF_H                   (0x04u)
#define TDC7200_REG_ADR_COARSE_CNTR_OVF_L                   (0x05u)
#define TDC7200_REG_ADR_CLOCK_CNTR_OVF_H                    (0x06u)
#define TDC7200_REG_ADR_CLOCK_CNTR_OVF_L                    (0x07u)
#define TDC7200_REG_ADR_CLOCK_CNTR_STOP_MASK_H              (0x08u)
#define TDC7200_REG_ADR_CLOCK_CNTR_STOP_MASK_L              (0x09u)
#define TDC7200_REG_ADR_TIME1                               (0x10u)
#define TDC7200_REG_ADR_CLOCK_COUNT1                        (0x11u)
#define TDC7200_REG_ADR_TIME2                               (0x12u)
#define TDC7200_REG_ADR_CLOCK_COUNT2                        (0x13u)
#define TDC7200_REG_ADR_TIME3                               (0x14u)
#define TDC7200_REG_ADR_CLOCK_COUNT3                        (0x15u)
#define TDC7200_REG_ADR_TIME4                               (0x16u)
#define TDC7200_REG_ADR_CLOCK_COUNT4                        (0x17u)
#define TDC7200_REG_ADR_TIME5                               (0x18u)
#define TDC7200_REG_ADR_CLOCK_COUNT5                        (0x19u)
#define TDC7200_REG_ADR_CLOCK_COUNTX(num)                   (TDC7200_REG_ADR_CLOCK_COUNT1 + 2*((num)-1))
#define TDC7200_REG_ADR_TIME6                               (0x1Au)
#define TDC7200_REG_ADR_TIMEX(num)                          (TDC7200_REG_ADR_TIME1 + 2*((num)-1))
#define TDC7200_REG_ADR_CALIBRATION1                        (0x1Bu)
#define TDC7200_REG_ADR_CALIBRATION2                        (0x1Cu)

#define TDC7200_REG_DEFAULTS_CONFIG2                        (0x40u)      // reset defaults
#define TDC7200_REG_DEFAULTS_INT_MASK                       (0x07u)      // reset defaults

#define TDC7200_REG_SHIFT_CONFIG1_FORCE_CAL                 (7u)
#define TDC7200_REG_SHIFT_CONFIG1_PARITY_EN                 (6u)
#define TDC7200_REG_SHIFT_CONFIG1_TRIGG_EDGE                (5u)
#define TDC7200_REG_SHIFT_CONFIG1_STOP_EDGE                 (4u)
#define TDC7200_REG_SHIFT_CONFIG1_START_EDGE                (3u)
#define TDC7200_REG_SHIFT_CONFIG1_MEAS_MODE                 (1u)
#define TDC7200_REG_SHIFT_CONFIG1_START_MEAS                (0u)

#define TDC7200_REG_VAL_CONFIG1_MEAS_MODE_MIN               (1u)
#define TDC7200_REG_VAL_CONFIG1_MEAS_MODE_MAX               (2u)
#define TDC7200_REG_VAL_CONFIG1_MEAS_MODE(num)              ((num)-1)

#define TDC7200_REG_SHIFT_CONFIG2_CALIBRATION2_PERIODS      (6u)
#define TDC7200_REG_SHIFT_CONFIG2_AVG_CYCLES                (3u)
#define TDC7200_REG_SHIFT_CONFIG2_NUM_STOP                  (0u)

#define TDC7200_REG_VAL_CONFIG2_CALIBRATION2_PERIODS_2      (0u)
#define TDC7200_REG_VAL_CONFIG2_CALIBRATION2_PERIODS_10     (1u)
#define TDC7200_REG_VAL_CONFIG2_CALIBRATION2_PERIODS_20     (2u)
#define TDC7200_REG_VAL_CONFIG2_CALIBRATION2_PERIODS_40     (3u)

#define TDC7200_REG_VAL_CONFIG2_AVG_CYCLES_MIN_VAL          (0u)
#define TDC7200_REG_VAL_CONFIG2_AVG_CYCLES_MIN              (1u << TDC7200_REG_VAL_CONFIG2_AVG_CYCLES_MIN_VAL)
#define TDC7200_REG_VAL_CONFIG2_AVG_CYCLES_MAX_VAL          (7u)
#define TDC7200_REG_VAL_CONFIG2_AVG_CYCLES_MAX              (1u << TDC7200_REG_VAL_CONFIG2_AVG_CYCLES_MAX_VAL)

#define TDC7200_REG_VAL_CONFIG2_NUM_STOP(num)               ( (num) - 1 )
#define TDC7200_REG_VAL_CONFIG2_NUM_STOP_MAX                (5)

#define TDC7200_REG_SHIFT_INT_STATUS_MEAS_COMPLETE_FLAG     (4u)
#define TDC7200_REG_SHIFT_INT_STATUS_MEAS_STARTED_FLAG      (3u)
#define TDC7200_REG_SHIFT_INT_STATUS_CLOCK_CNTR_OVF_INT     (2u)
#define TDC7200_REG_SHIFT_INT_STATUS_COARSE_CNTR_OVF_INT    (1u)
#define TDC7200_REG_SHIFT_INT_STATUS_NEW_MEAS_INT           (0u)

#define TDC7200_REG_SHIFT_INT_MASK_CLOCK_CNTR_OVF_MASK      (2u)
#define TDC7200_REG_SHIFT_INT_MASK_COARSE_CNTR_OVF_MASK     (1u)
#define TDC7200_REG_SHIFT_INT_MASK_NEW_MEAS_MASK            (0u)

#define TDC7200_REF_CLK_FREQ_HZ                             10000000
#define TDC7200_MAX_NUM_STOPS                               5
    
    
// Global Variables 
extern uint8_t _TDC7200_spi_tx_buffer[TDC7200_SPI_BUFFER_SIZE];
extern uint8_t _TDC7200_spi_rx_buffer[TDC7200_SPI_BUFFER_SIZE];


// Define functions
uint8_t TDC7200_Init( void );
uint8_t TDC7200_SetupMeasurement( const uint8_t cal2Periods, const uint8_t avgCycles, const uint8_t mode, const uint8_t numStops );
void TDC7200_SetupStopMask( const uint64_t stopMaskPs );
uint8_t TDC7200_SetupOverflow( const uint64_t overflowPs );
void TDC7200_StartMeasurement( );
uint64_t TDC7200_ReadMeasurement( const uint8_t stop_index );

//
void TDC7200_SpiTest( void );
uint8_t TDC7200_ReadReg8( const uint8_t reg_addr );
uint32_t TDC7200_ReadReg24( const uint8_t reg_addr );
uint8_t TDC7200_WriteReg8( const uint8_t reg_addr, const uint8_t reg_value );


#endif
/* [] END OF FILE */
