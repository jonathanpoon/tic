/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef I2C_H
#define I2C_H

#include "project.h"

typedef enum I2CS_BUFFER_STATUS
{
    I2CS_READ_BUFFER_FREE = 0,
    I2CS_READ_BUFFER_LOCKED = 1,
} I2CS_BUFFER_STATUS;
       
    
// Define Constants
#define I2CS_REG_SIZE                1u
#define I2CS_DATA_SIZE               16u
#define I2CS_BUFFER_SIZE             (I2CS_REG_SIZE + I2CS_DATA_SIZE)

//
extern uint8_t _I2CS_read_buffer[I2CS_BUFFER_SIZE];
extern uint8_t _I2CS_write_buffer[I2CS_BUFFER_SIZE];    

// Define functions
void I2C_Init( void );
void I2CS_ResetReadBuffer( uint8_t buffer_length );
void I2CS_ResetWriteBuffer( uint8_t buffer_length );
I2CS_BUFFER_STATUS I2CS_GetReadBufferStatus( void );
void I2CS_SetReadBufferStatus( I2CS_BUFFER_STATUS status );
void I2CS_SetReadBuffer( uint8_t* tmp_buffer, uint8_t tmp_buffer_length );
void I2CS_SetReadBufferValue( uint8_t value, uint8_t index );
#endif

/* [] END OF FILE */
