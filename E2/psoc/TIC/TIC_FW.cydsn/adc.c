/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "adc.h"

    
// ADC_DS - Global Variables
uint16_t _ADC_DS_buffer[ADC_DS_BUFFER_SIZE];
uint16_t _ADC_DS_voltage;

void ADC_Init( void )
{
    // Initialize Global variables
    ADC_DS_Start();
    // Disable the ADC_DS
    ADC_DS_StopConvert();
}


void ADC_DS_GetSamples( void )
{
    // Start DMA
    DMA_ADC_DS_Enable();
    // Start ADC Conversion
    ADC_DS_StartConvert();
    // Poll the status register to flag DMA completed
    do {} while ( ST_REG_DMA_ADC_DS_Read() != 1 );
    // Stop ADC Conversion
    ADC_DS_StopConvert();
}


void ADC_DS_GetMeanVoltage( void )
{    
    uint32_t j = 0;
    // Calculate the mean value of the ADC samples
    for( uint8_t i = 0; i < ADC_DS_BUFFER_SIZE; i++)
    {
        j += (uint16)_ADC_DS_buffer[i];
    }
       
    // Return the mean ADC value in units of volts
    _ADC_DS_voltage = ADC_DS_CountsTo_Volts( j / ADC_DS_BUFFER_SIZE );
}


/* [] END OF FILE */