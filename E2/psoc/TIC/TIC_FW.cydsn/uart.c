/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "uart.h"


// Global UART Variables
uint8 _UART_error_status;
uint8 _UART_rx_status;
uint8 _UART_rx_data;
uint8 _UART_rx_buffer_len;
char _UART_rx_buffer[UART_RX_BUFFER_SIZE];
char _UART_tx_buffer[UART_TX_BUFFER_SIZE];

#if defined(RUN_IN_DEVELOPMENT_MODE)

void UART_Init( void )
{
    // Initialize global variables
    // _UART_error_status = 0u;
    // _UART_rx_status = 0u;
    //  _UART_rx_data = 0u;
    // _UART_rx_buffer_len = 0u;
    memset( _UART_rx_buffer, '\0', UART_RX_BUFFER_SIZE );
    memset( _UART_tx_buffer, '\0', UART_TX_BUFFER_SIZE );

    UARTM_Start();
    UARTM_PutString( "Started TIC Controller Program\r\n" );
}


void UART_DisplayFrequency( float64 value )
{
    sprintf( _UART_tx_buffer, "Frequency: %.6f Hz \r\n", value );
    // UARTM_EnableTxInt();
    // DMA_UARTM_TX_Enable();
    // do { } while( ( ST_REG_DMA_UARTM_Read() & ST_REG_UARTM_TX_BITMASK ) != ST_REG_UARTM_TX_BITMASK );
    // UARTM_DisableTxInt();
    UARTM_PutString( _UART_tx_buffer );
}


/*
void UART_DisplayInputBuffer( void )
{
    // Send acknowledgement
    memset( _UART_tx_buffer, '\0', UART_TX_BUFFER_SIZE );
    sprintf( _UART_tx_buffer, "\r\n%d characters received --> ", _UART_rx_buffer_len );
    // Trigger DMA to send data    
    //UARTM_PutString( _UART_tx_buffer );

    //Echo received characters to verify data 
    for ( uint8_t _i = 0; _i < _UART_rx_buffer_len; _i++ )
    {
        UARTM_PutChar( _UART_rx_buffer[_i] );
    }
    UARTM_PutString( "\r\n" );
}
*/


/*
void UART_ProcessInterrupt()
{
    do
    {
        // Read receiver status register
        _UART_rx_status = UARTM_RXSTATUS_REG;

        if( ( _UART_rx_status & ( UARTM_RX_STS_BREAK | UARTM_RX_STS_PAR_ERROR | UARTM_RX_STS_STOP_ERROR | UARTM_RX_STS_OVERRUN ) ) != 0u)
        {
            // ERROR handling.
            _UART_error_status |= _UART_rx_status & ( UARTM_RX_STS_BREAK | UARTM_RX_STS_PAR_ERROR | UARTM_RX_STS_STOP_ERROR | UARTM_RX_STS_OVERRUN );
            
            //
            sprintf( _UART_tx_buffer, "\r\nERROR! %d characters received --> ", _UART_rx_buffer_len );
            UARTM_PutString( _UART_tx_buffer );
            UART_DisplayInputBuffer();
            // Reset the counter
            _UART_rx_buffer_len = 0;
            memset( _UART_rx_buffer, '\0', UART_INPUT_BUFFER_SIZE );
            break;
        }
        
        if ( ( _UART_rx_status & UARTM_RX_STS_FIFO_NOTEMPTY ) != 0u )
        {
            // Read data from the RX data register
            _UART_rx_data = UARTM_RXDATA_REG;
            if ( _UART_error_status == 0u )
            {
                // Send data backward
                UARTM_TXDATA_REG = _UART_rx_data;
                
                // Identify the stop character
                if ( _UART_rx_data == 'Z' )
                {
                    // Execute Command
                    if ( _UART_rx_buffer_len <= UART_INPUT_BUFFER_SIZE )
                    {
                        UART_DisplayInputBuffer();
                        // Process Event
                        
                        // DVDAC_SetValue( strtof( _UART_rx_buffer, NULL ) / 1000 );
                        // UART_DisplayMeasuredVoltage( ADC_DS_GetMeanVoltage() );
                    }
                    else
                    {
                        sprintf( _UART_tx_buffer, "Error! --> Need to supply %d characters\r\n", UART_INPUT_BUFFER_SIZE );
                        UARTM_PutString( _UART_tx_buffer );
                    }

                    // Reset the counter
                    _UART_rx_buffer_len = 0;
                    memset( _UART_rx_buffer, '\0', UART_INPUT_BUFFER_SIZE );
                }
                else
                {
                    _UART_rx_buffer[_UART_rx_buffer_len] = _UART_rx_data;
                    _UART_rx_buffer_len++;
                }
            }
        }
    }

    while ( ( _UART_rx_status & UARTM_RX_STS_FIFO_NOTEMPTY ) != 0u );   

}
*/

#endif 

/* [] END OF FILE */
