/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef _DATA_HANDLER_H_
#define _DATA_HANDLER_H_


#include "project.h"
#include "tic.h"
#include "ui.h"
#if defined(RUN_IN_DEVELOPMENT_MODE)
    #include "uart.h"
#endif

//
#define DH_NUM_TDC7200_CONTROL_REGISTERS           4u


typedef struct 
{
    uint8_t size;
	void* dataPtr;
} controlRegisterListItem_t;

typedef struct 
{
	uint8_t size;
	void* dataPtr;
} statusRegisterListItem_t;


typedef struct
{
	uint8_t TDC7200_calPeriods;
	uint8_t TDC7200_avgCycles;
	uint8_t TDC7200_mode;
    uint8_t TDC7200_num_stops;
    uint32_t TIC_clockDivider;
} controlRegisters_t;

extern controlRegisters_t gControlRegisters;
extern controlRegisterListItem_t gControlRegisterList[];
extern uint8_t gControlRegisterCount;


typedef struct
{
    float32 TDC7200_timeInterval_1;
    float32 TDC7200_timeInterval_2;
    float32 TDC7200_timeInterval_3;
    float32 TDC7200_timeInterval_4;
    float32 TDC7200_timeInterval_5;
} statusRegisters_t;

extern volatile statusRegisters_t gStatusRegisters;
extern statusRegisterListItem_t gStatusRegisterList[];
extern uint8_t gStatusRegisterCount;


// Define functions
void DATA_HANDLER_Init( void );
void DATA_HANDLER_InitControlRegisters( void );
void DATA_HANDLER_InitStatusRegisters( void );


#endif


/* [] END OF FILE */
