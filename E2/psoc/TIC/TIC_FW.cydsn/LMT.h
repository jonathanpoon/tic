/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include <math.h>

// LMT87 Defines
#define LMT70_A         -1.064200E-09
#define LMT70_B         -5.759725E-06
#define LMT70_C         -1.789883E-01
#define LMT70_D         2.048570E+02


// Function Defines
float32 LMT70_VoltageToTemperature( float32 voltage );
float32 LMT87_VoltageToTemperature( float32 voltage );


/* [] END OF FILE */
