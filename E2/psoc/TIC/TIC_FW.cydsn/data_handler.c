/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "data_handler.h"

// Global Variables
controlRegisters_t gControlRegisters;
volatile statusRegisters_t gStatusRegisters;


controlRegisterListItem_t gControlRegisterList[] = 
{
	{ sizeof(gControlRegisters.TDC7200_calPeriods), (void*)&gControlRegisters.TDC7200_calPeriods },
	{ sizeof(gControlRegisters.TDC7200_avgCycles), (void*)&gControlRegisters.TDC7200_avgCycles },
	{ sizeof(gControlRegisters.TDC7200_mode), (void*)&gControlRegisters.TDC7200_mode },
    { sizeof(gControlRegisters.TDC7200_num_stops), (void*)&gControlRegisters.TDC7200_num_stops },
	{ sizeof(gControlRegisters.TIC_clockDivider), (void*)&gControlRegisters.TIC_clockDivider },
};

// Calculate the number of elements in gControlRegisterList
uint8_t gControlRegisterCount = sizeof( gControlRegisterList ) / sizeof( controlRegisterListItem_t );


statusRegisterListItem_t gStatusRegisterList[] = 
{
	{ sizeof(gStatusRegisters.TDC7200_timeInterval_1), (void*)&gStatusRegisters.TDC7200_timeInterval_1 },
    { sizeof(gStatusRegisters.TDC7200_timeInterval_2), (void*)&gStatusRegisters.TDC7200_timeInterval_2 },
    { sizeof(gStatusRegisters.TDC7200_timeInterval_3), (void*)&gStatusRegisters.TDC7200_timeInterval_3 },
    { sizeof(gStatusRegisters.TDC7200_timeInterval_4), (void*)&gStatusRegisters.TDC7200_timeInterval_4 },
    { sizeof(gStatusRegisters.TDC7200_timeInterval_5), (void*)&gStatusRegisters.TDC7200_timeInterval_5 },
};

// Calculate the number of elements in gStatusRegisterList
uint8_t gStatusRegisterCount = sizeof( gStatusRegisterList ) / sizeof( statusRegisterListItem_t );


void DATA_HANDLER_Init( void )
{
    DATA_HANDLER_InitControlRegisters();
    DATA_HANDLER_InitStatusRegisters();   
}


void DATA_HANDLER_InitControlRegisters( void )
{   
    // Initialize control registers to NULL
    // memset( (uint8*)&gControlRegisters, '\0', sizeof( controlRegisters_t ) );

    // Initialize to default values
	gControlRegisters.TDC7200_calPeriods = TDC7200_DEFAULT_CAL_PERIODS;
	gControlRegisters.TDC7200_avgCycles = TDC7200_DEFAULT_AVG_CYCLES;
	gControlRegisters.TDC7200_mode = TDC7200_DEFAULT_MODE;
    gControlRegisters.TDC7200_num_stops = TDC7200_DEFAULT_NUM_STOPS;
	gControlRegisters.TIC_clockDivider = TIC_DEFAULT_CLK_DIVIDER;
}


void DATA_HANDLER_InitStatusRegisters( void )
{
    // Set all StatusRegisters_t to 0
    memset( (uint8*)&gStatusRegisters, 0.0f, sizeof( statusRegisters_t ) );    
}


// [] END OF FILE
