/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/


#include "spi.h"


void SPI_Init( void )
{
    // Start SPIM
    SPI_TDC7200_DisableInt();
    SPI_TDC7200_Start();
}


void SPI_TDC7200_TransmitReceive( )
{
    // Clear SPIM FIFO Buffers
    SPI_TDC7200_ClearFIFO();
    //
    SPI_TDC7200_EnableInt();
    // Enable DMA_SPI_TDC7200_TX and DMA_SPI_TDC7200_RX
    DMA_SPI_TDC7200_RX_Enable();
    DMA_SPI_TDC7200_TX_Enable();

    // Poll the STATUS_REG to finish RX read (Bit 0)
    // do { _ST_REG_value = ST_REG_DMA_SPI_TDC7200_Read(); } while( ( _ST_REG_value & ST_REG_DMA_SPI_TDC7200_RX_BITMASK ) != ST_REG_DMA_SPI_TDC7200_RX_BITMASK );
    do { } while( ( ST_REG_DMA_SPI_TDC7200_Read() & ST_REG_DMA_SPI_TDC7200_RX_BITMASK ) != ST_REG_DMA_SPI_TDC7200_RX_BITMASK );
    // Disable Interrupt on SPI
    SPI_TDC7200_DisableInt();
}


void SPI_TDC7200_TransmitReceive16( )
{
    DMA_SPI_TDC7200_TX_TdConfig( 2u );
    DMA_SPI_TDC7200_RX_TdConfig( 2u );
    SPI_TDC7200_TransmitReceive();
}


void SPI_TDC7200_TransmitReceive32( )
{
    DMA_SPI_TDC7200_TX_TdConfig( 4u );
    DMA_SPI_TDC7200_RX_TdConfig( 4u );
    SPI_TDC7200_TransmitReceive();
}


/* [] END OF FILE */
