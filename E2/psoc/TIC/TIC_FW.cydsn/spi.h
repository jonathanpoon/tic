/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef SPI_H
#define SPI_H
    
#include "project.h"
#include "global_defs.h"
#include "dma.h"
#include "tdc7200.h"
    

/* spi.h Defines */
#define ST_REG_DMA_SPI_TDC7200_RX_BITMASK               0b01
#define ST_REG_DMA_SPI_TDC7200_TX_BITMASK               0b10

    
// Function Defines
void SPI_Init( void );
void SPI_TDC7200_TransmitReceive( void );
void SPI_TDC7200_Transmit( void );

void SPI_TDC7200_TransmitReceive16( void );
void SPI_TDC7200_TransmitReceive32( void );

#endif

/* [] END OF FILE */
