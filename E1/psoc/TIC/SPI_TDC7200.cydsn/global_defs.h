/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef GLOBAL_DEFS_H
#define GLOBAL_DEFS_H


#define LOW                                     0
#define HIGH                                    1
#define DISABLE                                 0
#define ENABLE                                  1
#define FALSE                                   0
#define TRUE                                    1


#define TDC_BUFFER_SIZE                         (4u)


#define STR_DMA_SPI_TDC_RX_BITMASK              0b01
#define STR_DMA_SPI_TDC_TX_BITMASK              0b10
    
/* DMA Configuration for DMA_SPI_TDC_TX */
#define DMA_SPI_TDC_TX_BYTES_PER_BURST          (1u)
#define DMA_SPI_TDC_TX_REQUEST_PER_BURST        (1u)
#define DMA_SPI_TDC_TX_SRC_BASE                 (CYDEV_SRAM_BASE)
#define DMA_SPI_TDC_TX_DST_BASE                 (CYDEV_PERIPH_BASE)

/* DMA Configuration for DMA_SPI_TDC_RX */
#define DMA_SPI_TDC_RX_BYTES_PER_BURST          (1u)
#define DMA_SPI_TDC_RX_REQUEST_PER_BURST        (1u)
#define DMA_SPI_TDC_RX_SRC_BASE                 (CYDEV_PERIPH_BASE)
#define DMA_SPI_TDC_RX_DST_BASE                 (CYDEV_SRAM_BASE)


#endif
    
/* [] END OF FILE */
