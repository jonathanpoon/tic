/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef SPI_H
#define SPI_H


#include "project.h"
#include "global_defs.h"
#include "dma.h"
#include "tdc.h"

//
void SPI_Init( void );
void SPI_TDC_Transmit( );
void SPI_TDC_TransmitReceive( );


#endif

/* [] END OF FILE */
