/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef TDC_H
#define TDC_H


#include "project.h"
#include "spi.h"
    
//
void TDC_Init( void );
void TDC_Update( void );

#endif
/* [] END OF FILE */
