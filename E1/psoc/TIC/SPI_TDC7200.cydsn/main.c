/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/ 
#include <stdio.h>
#include "project.h"
#include "global_defs.h"
#include "spi.h"
#include "dma.h"
#include "tdc.h"

// #include "isr.h"

//

uint8_t value;

int main(void)
{
    /* Enable global interrupts. */
    CyGlobalIntEnable; 
    
    SPI_Init();
    DMA_Init();
    TDC_Init();

    SPI_TDC_Transmit();
    SPI_TDC_TransmitReceive();
    // uint8_t SPI_data[4] = { 1, 0, 0, 0 };
    // char newline[2] = { '\r', '\n' };
    TDC_Update();
    TDC_Update();
    TDC_Update();
    //
    while ( 1 )
    {
        
        /* Test SPI Transmit to SHR
        for ( uint8_t i = 0; i < 8; i++ )
        {
            SPI_TDC_Transmit( SPI_data );
            CyDelay( 100 );
            SPI_TDC_TransmitReceive( SPI_data );
            SPI_data[0]++;
        }
        */
        // UARTM_PutString( _SPI_TDC_rx_buffer );
        //UARTM_PutString( newline );
    }
}

/* [] END OF FILE */
