/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "spi.h"

// Global Variables
uint8_t _SPI_TDC_tx_intr_disable_mask_reg;
uint8_t _SPI_TDC_rx_intr_disable_mask_reg;
uint8_t _ST_REG_value;


void SPI_Init( void )
{
    // Disable the TX interrupt on empty for SPIM
    SPI_TDC_TX_STATUS_MASK_REG &= (~SPI_TDC_INT_ON_TX_NOT_FULL);
    // Save SPI_TDC_TX_STATUS_MASK_REG into variable to disable the TX interrupt using DMA */
    _SPI_TDC_tx_intr_disable_mask_reg = SPI_TDC_TX_STATUS_MASK_REG;

    // Disable the RX interrupt on full for SPIM
    SPI_TDC_RX_STATUS_MASK_REG &= (~SPI_TDC_INT_ON_RX_NOT_EMPTY);
    // Save SPI_TDC_RX_STATUS_MASK_REG into variable to disable the RX interrupt using DMA */
    _SPI_TDC_rx_intr_disable_mask_reg = SPI_TDC_RX_STATUS_MASK_REG;

    // Start SPIM
    SPI_TDC_Start();
}


void SPI_TDC_Transmit()
{
    // Clear SPIM FIFO Buffers
    SPI_TDC_ClearFIFO();
    // Enable DMA_SPI_TDC_TX
    DMA_SPI_TDC_TX_Enable();
    // Generate interrupt on SPI_TDC_INT_ON_TX_NOT_FULL  
    SPI_TDC_TX_STATUS_MASK_REG |= SPI_TDC_INT_ON_TX_NOT_FULL;
    // Poll the STATUS_REG to finish TX read (Bit 1)
    do { } while( ( STR_SPI_TDC_Read() & STR_DMA_SPI_TDC_TX_BITMASK ) != STR_DMA_SPI_TDC_TX_BITMASK );
}


void SPI_TDC_TransmitReceive( )
{
    // Clear SPIM FIFO Buffers
    SPI_TDC_ClearFIFO();
    // Enable DMA_SPI_TDC_TX and DMA_SPI_TDC_RX
    DMA_SPI_TDC_RX_Enable();
    DMA_SPI_TDC_TX_Enable();
    // Generate interrupt on SPI_TDC_INT_ON_RX_NOT_EMPTY
    SPI_TDC_RX_STATUS_MASK_REG |= SPI_TDC_INT_ON_RX_NOT_EMPTY;
    // Generate interrupt on SPI_TDC_INT_ON_TX_NOT_FULL  
    SPI_TDC_TX_STATUS_MASK_REG |= SPI_TDC_INT_ON_TX_NOT_FULL;
    // Poll the STATUS_REG to finish RX read (Bit 0)
    do { _ST_REG_value = STR_SPI_TDC_Read(); } while( ( _ST_REG_value & STR_DMA_SPI_TDC_RX_BITMASK ) != STR_DMA_SPI_TDC_RX_BITMASK );
    // do { } while( ( STR_SPI_TDC_Read() & STR_DMA_SPI_TDC_RX_BITMASK ) != STR_DMA_SPI_TDC_RX_BITMASK );
}


/* [] END OF FILE */
