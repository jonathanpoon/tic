/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef GLOBAL_DEFS_H
#define GLOBAL_DEFS_H


#define LOW                                         0
#define HIGH                                        1
#define DISABLE                                     0
#define ENABLE                                      1
#define FALSE                                       0
#define TRUE                                        1
#define BIT(i)                                      (0x01 << i)

    
#endif
    
/* [] END OF FILE */
