/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "dma.h"

//
uint8_t _DMA_SPI_TDC7200_TX_channel, _DMA_SPI_TDC7200_RX_channel;
uint8_t _DMA_SPI_TDC7200_TX_td[1], _DMA_SPI_TDC7200_RX_td[1];
//
uint8_t _DMA_ADC_DS_channel, _DMA_ADC_DS_td[2];
//
uint8_t _DMA_UARTM_TX_channel, _DMA_UARTM_RX_channel;
uint8_t _DMA_UARTM_TX_td[1], _DMA_UARTM_RX_td[1];


void DMA_Init( void )
{
    DMA_SPI_TDC7200_TX_Init();
    DMA_SPI_TDC7200_RX_Init();
    DMA_ADC_DS_Init();
    // DMA_UART_Init();
}


/* ------------------------------------------------------------------------------------- */

void DMA_SPI_TDC7200_TX_Init()
{
    // Init DMA Channel, 1 byte bursts, each burst requires a request */ 
    _DMA_SPI_TDC7200_TX_channel = DMA_SPI_TDC7200_TX_DmaInitialize( DMA_SPI_TDC7200_TX_BYTES_PER_BURST, DMA_SPI_TDC7200_TX_REQUEST_PER_BURST, HI16(DMA_SPI_TDC7200_TX_SRC_BASE), HI16(DMA_SPI_TDC7200_TX_DST_BASE) );
    // Allocate TD
    _DMA_SPI_TDC7200_TX_td[0] = CyDmaTdAllocate();
    // Configure TD
    DMA_SPI_TDC7200_TX_TdConfig( TDC7200_SPI_BUFFER_SIZE );
    // From the memory to the SPIM
    CyDmaTdSetAddress( _DMA_SPI_TDC7200_TX_td[0], LO16((uint32)_TDC7200_spi_tx_buffer), LO16((uint32)SPI_TDC7200_TXDATA_PTR) );
}


void DMA_SPI_TDC7200_TX_TdConfig( uint8_t buffer_size )
{
    CyDmaTdSetConfiguration( _DMA_SPI_TDC7200_TX_td[0], buffer_size, DMA_DISABLE_TD, DMA_SPI_TDC7200_TX__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR );
}


void DMA_SPI_TDC7200_TX_Enable()
{
    // Associate the TD with the channel
    CyDmaChSetInitialTd( _DMA_SPI_TDC7200_TX_channel, _DMA_SPI_TDC7200_TX_td[0] );
    // Enable the SPI_TDC7200_TX_DMA_channel
    CyDmaChEnable( _DMA_SPI_TDC7200_TX_channel, ENABLE );
}


/* ------------------------------------------------------------------------------------- */

void DMA_SPI_TDC7200_RX_Init()
{ 
    // Init DMA Channel, 1 byte bursts, each burst requires a request
    _DMA_SPI_TDC7200_RX_channel = DMA_SPI_TDC7200_RX_DmaInitialize( DMA_SPI_TDC7200_RX_BYTES_PER_BURST, DMA_SPI_TDC7200_RX_REQUEST_PER_BURST, HI16(DMA_SPI_TDC7200_RX_SRC_BASE), HI16(DMA_SPI_TDC7200_RX_DST_BASE) );
    // Allocate TD
    _DMA_SPI_TDC7200_RX_td[0] = CyDmaTdAllocate();
    // Configure TD
    DMA_SPI_TDC7200_RX_TdConfig( TDC7200_SPI_BUFFER_SIZE );
    /* From the SPIM RX Buffer to SRAM */
    CyDmaTdSetAddress( _DMA_SPI_TDC7200_RX_td[0], LO16((uint32)SPI_TDC7200_RXDATA_PTR), LO16((uint32)_TDC7200_spi_rx_buffer) );
}


void DMA_SPI_TDC7200_RX_TdConfig( uint8_t buffer_size )
{
    CyDmaTdSetConfiguration( _DMA_SPI_TDC7200_RX_td[0], buffer_size, DMA_DISABLE_TD, DMA_SPI_TDC7200_RX__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR );
}


void DMA_SPI_TDC7200_RX_Enable()
{
    // Associate the TD with the channel 
    CyDmaChSetInitialTd( _DMA_SPI_TDC7200_RX_channel, _DMA_SPI_TDC7200_RX_td[0] );
    // Enable the SPI_TDC7200_TX_DMA_channel
    CyDmaChEnable( _DMA_SPI_TDC7200_RX_channel, ENABLE );
}


/* ------------------------------------------------------------------------------ */

void DMA_ADC_DS_Init()
{
    // Iniitialize DMA channel
    _DMA_ADC_DS_channel = DMA_ADC_DS_DmaInitialize( DMA_ADC_DS_BYTES_PER_BURST, DMA_ADC_DS_REQUEST_PER_BURST, HI16(CYDEV_PERIPH_BASE), HI16(CYDEV_SRAM_BASE) );
    // Allocate TD
    _DMA_ADC_DS_td[0] = CyDmaTdAllocate();
    // Configure TD - Increment SRC address to transfer 16-bits from ADC using 16-bit DMA spoke
    CyDmaTdSetConfiguration( _DMA_ADC_DS_td[0], ADC_DS_BUFFER_SIZE * DMA_ADC_DS_BYTES_PER_BURST, DMA_DISABLE_TD, DMA_ADC_DS__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR );
    // Set Source and Destination address
    CyDmaTdSetAddress( _DMA_ADC_DS_td[0], LO16((uint32)ADC_DS_DEC_SAMP_PTR), LO16((uint32)&_ADC_DS_buffer) );
}


void DMA_ADC_DS_Enable()
{
    // TD initialization
    CyDmaChSetInitialTd( _DMA_ADC_DS_channel, _DMA_ADC_DS_td[0] );
    // 1u the DMA channel
    CyDmaChEnable( _DMA_ADC_DS_channel, ENABLE );
}


/* ------------------------------------------------------------------------------ */

#if defined(RUN_IN_DEVELOPMENT_MODE)
void DMA_UARTM_TX_Init( void )
{
     // Init DMA Channel, 1 byte bursts, each burst requires a request 
    _DMA_UARTM_TX_channel = DMA_UARTM_TX_DmaInitialize( DMA_UARTM_TX_BYTES_PER_BURST, DMA_UARTM_TX_REQUEST_PER_BURST, HI16(DMA_UARTM_TX_SRC_BASE), HI16(DMA_UARTM_TX_DST_BASE) );
    // Allocate TD
    _DMA_UARTM_TX_td[0] = CyDmaTdAllocate();
    // Configure the TD
    DMA_UARTM_TX_TdConfig( UARTM_TX_BUFFER_SIZE );
    // From the memory to the SPIM
    CyDmaTdSetAddress( _DMA_UARTM_TX_td[0], LO16((uint32)_UARTM_tx_buffer), LO16((uint32)UARTM_TXDATA_PTR) );
}


void DMA_UARTM_TX_TdConfig( uint8_t buffer_size )
{
    CyDmaTdSetConfiguration( _DMA_UARTM_TX_td[0], buffer_size, DMA_DISABLE_TD, DMA_UARTM_TX__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR );
}


void DMA_UARTM_TX_Enable( void )
{
    // Associate the TD with the channel
    CyDmaChSetInitialTd( _DMA_UARTM_TX_channel, _DMA_UARTM_TX_td[0] );
    // Enable the SPI_TDC7200_TX_DMA_channel
    CyDmaChEnable( _DMA_UARTM_TX_channel, ENABLE );
}
#endif


/* [] END OF FILE */
