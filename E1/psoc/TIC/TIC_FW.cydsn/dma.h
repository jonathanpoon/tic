/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef DMA_H
#define DMA_H
    
#include "project.h"
#include "global_defs.h"
#include "spi.h"
#include "tdc7200.h"
#include "adc.h"
#if defined(RUN_IN_DEVELOPMENT_MODE)
    #include "uart.h"    
#endif

/* DMA Configuration for DMA_SPI_TDC7200_TX */ 
#define DMA_UARTM_TX_BYTES_PER_BURST                    (1u)
#define DMA_UARTM_TX_REQUEST_PER_BURST                  (1u)
#define DMA_UARTM_TX_SRC_BASE                           (CYDEV_SRAM_BASE)
#define DMA_UARTM_TX_DST_BASE                           (CYDEV_PERIPH_BASE)    
    
    /* DMA Configuration for DMA_SPI_TDC7200_TX */
#define DMA_SPI_TDC7200_TX_BYTES_PER_BURST              (1u)
#define DMA_SPI_TDC7200_TX_REQUEST_PER_BURST            (1u)
#define DMA_SPI_TDC7200_TX_SRC_BASE                     (CYDEV_SRAM_BASE)
#define DMA_SPI_TDC7200_TX_DST_BASE                     (CYDEV_PERIPH_BASE)

/* DMA Configuration for DMA_SPI_TDC7200_RX */
#define DMA_SPI_TDC7200_RX_BYTES_PER_BURST              (1u)
#define DMA_SPI_TDC7200_RX_REQUEST_PER_BURST            (1u)
#define DMA_SPI_TDC7200_RX_SRC_BASE                     (CYDEV_PERIPH_BASE)
#define DMA_SPI_TDC7200_RX_DST_BASE                     (CYDEV_SRAM_BASE)

/* DMA Configuration for ADC */
#define DMA_ADC_DS_BYTES_PER_BURST                      (2u)
#define DMA_ADC_DS_REQUEST_PER_BURST                    (1u)
    
    
// Define functions
void DMA_Init( void );

// Define SPI_TDC DMA functions
void DMA_SPI_TDC7200_TX_Init( void );
void DMA_SPI_TDC7200_RX_Init( void );
void DMA_SPI_TDC7200_TX_TdConfig( uint8_t buffer_size );
void DMA_SPI_TDC7200_RX_TdConfig( uint8_t buffer_size );
void DMA_SPI_TDC7200_TX_Enable( void );
void DMA_SPI_TDC7200_RX_Enable( void );    

// Define ADC_DS DMA functions
void DMA_ADC_DS_Init( void );
void DMA_ADC_DS_Enable( void );

#if defined(RUN_IN_DEVELOPMENT_MODE)
    // Define UARTM DMA functions
    void DMA_UARTM_TX_Init( void );
    void DMA_UARTM_TX_TdConfig( uint8_t buffer_size );
    void DMA_UARTM_TX_Enable( void );
#endif

#endif

/* [] END OF FILE */
