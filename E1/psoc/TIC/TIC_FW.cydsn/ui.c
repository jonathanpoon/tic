/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "ui.h"

// Define Global Variables
UI_Control_t _UI_Control;
UI_StatusFlag_t _UI_StatusFlags;


void UI_Init( void )
{
	_UI_Control.i2cRegAddress = 0xFF;			// 
	_UI_Control.i2cWriteDataLength = 0x00; 		// 
	_UI_Control.i2cReadDataLength = 0x00;       // 
    
	_UI_Control.regIndex = 0xFF;			    // 
	_UI_Control.regDataPtr = (uint8_t*)NULL; 	// 
	_UI_Control.regDataLength = 0x00;           // 
    
	_UI_Control.commandOption = 0xFF;			// 
	memset( _UI_Control.commandDataBuffer, 0xFF, CMD_OPTIONS_SIZE );  // 
	_UI_Control.regDataLength = 0x00;           //
    
    _UI_Control.streamingEnabled = 0x00;
}


void UI_Run( void )
{    
    // Read complete: parse command packet
    if( I2CS_SlaveStatus() & I2CS_SSTAT_RD_CMPLT )
    {
        #if defined(RUN_IN_DEVELOPMENT_MODE)
            // Display I2CS Read Buffer on UART
            UART_DisplayByteBuffer( "I2CS Read Buffer: ", _I2CS_read_buffer, I2CS_SlaveGetReadBufSize() );
        #endif
        //
//        if ( I2CS_SlaveGetReadBufSize() == _UI_Control.i2cReadDataLength )
//        {
            // Clear Slave Read buffer
            I2CS_ResetReadBuffer( _UI_Control.i2cReadDataLength );
             // Initialize Read buffer
            I2CS_SlaveInitReadBuf( _I2CS_read_buffer, I2CS_BUFFER_SIZE );
//        }
        // Release Slave buffer
        I2CS_SetReadBufferStatus( I2CS_READ_BUFFER_FREE );
        // Resets the read buffer counter to zero 
        I2CS_SlaveClearReadBuf();
        // Clears the slave read status flags.
        I2CS_SlaveClearReadStatus();
    }
       
    // Write complete: parse command packet
    if ( I2CS_SlaveStatus() & I2CS_SSTAT_WR_CMPLT )
    {
        #if defined(RUN_IN_DEVELOPMENT_MODE)
            // Display I2CS Write Buffer on UART
            UART_DisplayByteBuffer( "I2CS Write Buffer: ", _I2CS_write_buffer, I2CS_SlaveGetWriteBufSize() );
        #endif

        // Resest UI Status Flags
        UI_ResetStatusFlags();
        
        // Parse Write Request
        if ( UI_ParseWriteRequest() )
        {
            UI_ProcessWriteRequest();
        }
        // Update status flag on _I2CS_read_buffer to invalid I2C command
        else
        {
            // _UI_Control.i2cReadDataLength = 1;
            // memset( _I2CS_read_buffer, 0xFF, _UI_Control.i2cReadDataLength );
        }
        
        // Resets the read buffer counter to zero.
        I2CS_SlaveClearWriteBuf();
        // Clears the slave write status flags.
        I2CS_SlaveClearWriteStatus();
        // Initialize Write buffer
        I2CS_SlaveInitWriteBuf( _I2CS_write_buffer, I2CS_BUFFER_SIZE );
        I2CS_ResetWriteBuffer( I2CS_BUFFER_SIZE );
    }
}


uint8_t UI_ParseWriteRequest( void )
{
    // Invalid Write Request
    if ( I2CS_SlaveGetWriteBufSize() == 0 ) {   return 0;   }
    
    // Set the address information
    _UI_Control.i2cRegAddress = _I2CS_write_buffer[0];
    _UI_Control.i2cWriteDataLength = I2CS_SlaveGetWriteBufSize() - 1;

    #if defined(RUN_IN_DEVELOPMENT_MODE)
        UART_DisplayByteBuffer( "I2C Buffer: ", (uint8_t[]){ _UI_Control.i2cRegAddress, _UI_Control.i2cWriteDataLength }, 2 );
    #endif


    // Control Registers are R/W
	if ( (_UI_Control.i2cRegAddress >= CONTROL_REG_START_ADDR) && (_UI_Control.i2cRegAddress < (CONTROL_REG_START_ADDR + gControlRegisterCount)) )
    {
        // Copy Memory location
        _UI_Control.regIndex = _UI_Control.i2cRegAddress - CONTROL_REG_START_ADDR;
        _UI_Control.regDataPtr = (uint8_t*)( gControlRegisterList[_UI_Control.regIndex].dataPtr );	
        _UI_Control.regDataLength = gControlRegisterList[_UI_Control.regIndex].size;
        //
        #if defined(RUN_IN_DEVELOPMENT_MODE)
            UART_DisplayByteBuffer( "Control Register: ", (uint8_t[]){ _UI_Control.regIndex, _UI_Control.regDataLength }, 2 );
        #endif
        // Set status flags
        _UI_StatusFlags.controlRegisterAccess = 1;
        _UI_StatusFlags.readShadow = 1;
        // Check data length to verify write request
        if ( _UI_Control.i2cWriteDataLength > 0 )
        {
            if ( _UI_Control.i2cWriteDataLength == _UI_Control.regDataLength )
            { 
                _UI_StatusFlags.writeShadow = 1;
            }
            else
            {
                _UI_StatusFlags.readShadow = 0;
                return 0;
            }
        } 
	}
    // Status Registers - Read Only
	else if ( (_UI_Control.i2cRegAddress >= STATUS_REG_START_ADDR) && (_UI_Control.i2cRegAddress < (STATUS_REG_START_ADDR + gStatusRegisterCount) ))
	{
        // Copy Memory location
        _UI_Control.regIndex = _UI_Control.i2cRegAddress - STATUS_REG_START_ADDR;
        _UI_Control.regDataPtr = (uint8_t*)(gStatusRegisterList[_UI_Control.regIndex].dataPtr);	
		_UI_Control.regDataLength = gStatusRegisterList[_UI_Control.regIndex].size;
        //
        #if defined(RUN_IN_DEVELOPMENT_MODE)
            UART_DisplayByteBuffer( "Status Register: ", (uint8_t[]){ _UI_Control.regIndex, _UI_Control.regDataLength }, 2 );
        #endif
        // Set status flags
        _UI_StatusFlags.statusRegisterAccess = 1;
        _UI_StatusFlags.readShadow = 1;
        // Check data length to verify read request
        if ( _UI_Control.i2cWriteDataLength != 0 )
        {
            _UI_StatusFlags.readShadow = 0;
            return 0;
        }   
	}
    // Command Registers - Execute command with data
	else if ( (_UI_Control.i2cRegAddress >= COMMAND_REG_START_ADDR) && (_UI_Control.i2cRegAddress < (COMMAND_REG_START_ADDR + CMD_OPTIONS_SIZE ) ) )
	{
        // Copy Memory location
        _UI_Control.commandOption = _UI_Control.i2cRegAddress - COMMAND_REG_START_ADDR;
        //
		switch ( _UI_Control.commandOption )
		{
			case CMD_MEASURE_TIME_INTERVAL:
				_UI_Control.commandDataLength = sizeof( uint8_t );
				break;
			case CMD_MEASURE_VOLTAGE:
				_UI_Control.commandDataLength = sizeof( _ADC_DS_voltage );
				break;
			default:
				_UI_Control.commandDataLength = 0;
		};
        // Set status flags
        _UI_StatusFlags.executeCommand = 1;
        // Check data length to verify write request
        if ( _UI_Control.i2cWriteDataLength > 0 )
        {
            if ( _UI_Control.i2cWriteDataLength != _UI_Control.commandDataLength )
            { 
                return 0;
            }
            // Copy Data to Command data buffer
            memcpy( _UI_Control.commandDataBuffer, _I2CS_write_buffer + 1, _UI_Control.commandDataLength );
        }
        #if defined(RUN_IN_DEVELOPMENT_MODE)
            UART_DisplayByteBuffer( "Command Register: ", (uint8_t[]){ _UI_Control.commandOption, _UI_Control.commandDataLength }, 2 );
        #endif
	}
    // Invalid Write request
	else
	{
        //
        #if defined(RUN_IN_DEVELOPMENT_MODE)
            UARTM_PutString( "Invalid I2C Register Selected\n\r" );
        #endif
        //
        return 0;
	}

    // Valid request TRUE
    return 1;
}


void UI_ProcessWriteRequest( void )
{
    // Copy from I2C Read Buffer to Shadow
    if ( _UI_StatusFlags.writeShadow )
	{	
        // Bypass first element of _I2CS_write_buffer
        memcpy( _UI_Control.regDataPtr, _I2CS_write_buffer + 1, _UI_Control.i2cWriteDataLength );
        #if defined(RUN_IN_DEVELOPMENT_MODE)
            UART_DisplayByteBuffer( "Writing Shadow Register: ", _UI_Control.regDataPtr, _UI_Control.i2cWriteDataLength );
        #endif
        
        if ( _UI_StatusFlags.controlRegisterAccess )
        {
            // Update TDC7200 when relevant registers are updated
            if ( _UI_Control.regIndex >= 0 && _UI_Control.regIndex < DH_NUM_TDC7200_CONTROL_REGISTERS )
            {
                TDC7200_SetupMeasurement( gControlRegisters.TDC7200_calPeriods, gControlRegisters.TDC7200_avgCycles, gControlRegisters.TDC7200_mode, gControlRegisters.TDC7200_num_stops );
            }
        }
	}
    // Copy from Shadow to I2C Read Buffer
    if ( _UI_StatusFlags.readShadow )
    {
        //
        UI_UpdateI2CReadBuffer( _UI_Control.regDataPtr, _UI_Control.regDataLength );
        //
        #if defined(RUN_IN_DEVELOPMENT_MODE)
            UART_DisplayByteBuffer( "Read Shadow Register: ", _UI_Control.regDataPtr, _UI_Control.regDataLength );
        #endif
    }
    // Command
	if ( _UI_StatusFlags.executeCommand )
	{
		UI_ExecuteCommand();
	}
}


uint8_t UI_ExecuteCommand( void )
{
    #if defined(RUN_IN_DEVELOPMENT_MODE)
        UART_DisplayByteBuffer( "Executing Command: ", (uint8_t[]){ _UI_Control.commandOption, _UI_Control.commandDataLength }, 2 );
    #endif
    
    // Execute command based on UI_CommandOptions index
	switch ( _UI_Control.commandOption )
	{
		case CMD_MEASURE_TIME_INTERVAL:
			// Execute command
            TIC_MeasureTimeInterval();
            // Write to _I2CS_read_buffer
            UI_UpdateI2CReadBuffer( (uint8_t[]){ 0x01 }, _UI_Control.commandDataLength );
			break;        
		case CMD_MEASURE_VOLTAGE:
			// Execute command
            ADC_DS_GetMeanVoltage();
            // Write to _I2CS_read_buffer
            UI_UpdateI2CReadBuffer( (uint8_t*)&_ADC_DS_voltage, _UI_Control.commandDataLength );
			break;
		default:
            #if defined(RUN_IN_DEVELOPMENT_MODE)
                UARTM_PutString( "Invalid Command\n\r" );
            #endif            
            
            // Write status flag to _I2CS_read_buffer
            UI_UpdateI2CReadBuffer( (uint8_t[]){ 0xFF }, 1 );
            return 0;		
	};
    //
    return 1;
}


void UI_UpdateI2CReadBuffer( uint8_t* dataBuffer, uint8_t dataLength )
{
    // Do not Update the I2C read buffer only if streaming is enabled
    if ( !_UI_Control.streamingEnabled && ( I2CS_GetReadBufferStatus() == I2CS_READ_BUFFER_FREE ) )
    {
        // Update the i2c_read_buffer_length
        _UI_Control.i2cReadDataLength = dataLength;
        // Copy data to the I2CS_read_buffer
        I2CS_SetReadBuffer( dataBuffer, _UI_Control.i2cReadDataLength );
        // Lock the I2CS_read_buffer 
        I2CS_SetReadBufferStatus( I2CS_READ_BUFFER_LOCKED );
        // 
        #if defined(RUN_IN_DEVELOPMENT_MODE)
            UART_DisplayByteBuffer( "Copy to I2CS Read Buffer: ", _I2CS_read_buffer, _UI_Control.i2cReadDataLength );
        #endif
    }
}


void UI_ResetStatusFlags( void )
{
    _UI_StatusFlags.writeShadow = 0;
	_UI_StatusFlags.readShadow = 0;
    _UI_StatusFlags.executeCommand = 0;
    _UI_StatusFlags.controlRegisterAccess = 0;
    _UI_StatusFlags.statusRegisterAccess = 0;
}


// [] END OF FILE */
