/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "tic.h"
#include "data_handler.h"


double _TIC_time_interval_measurement[TDC7200_REG_VAL_CONFIG2_NUM_STOP_MAX];


void TIC_Init( void )
{   
    // Enable Ripple Counter Output
    RC_OE_EN_Write( LOW );
    // Enable Ripple Counter 
    RC_EN_Write( LOW );
    // Reset 
    RC_MRC_Write( LOW );
    // Enable Regulator
    VDDA_REG_EN_Write( HIGH );    
    // Regulator Startup Delay
    CyDelay( 10 );
    // Initialize the TDC
    TDC7200_Init();
    // Setup TDC for time interval measurement
    TDC7200_SetupMeasurement( gControlRegisters.TDC7200_calPeriods, gControlRegisters.TDC7200_avgCycles, gControlRegisters.TDC7200_mode, gControlRegisters.TDC7200_num_stops );
}


void TIC_MeasureTimeInterval( void )
{   
    // Reset interrupt status in TDC7200 and trigger start measurement
    TDC7200_StartMeasurement();
    // Disable reset - Trigger analog circuit to start
    RC_MRC_Write( HIGH );
    //
    CyDelay( 10 );
    // Poll TDC7200_INTB interrupt from TDC7200 to wait for completed 
    do { } while ( ST_REG_TDC7200_INTB_Read() != 1 );
    // Clear GPIO Interrupt
    TDC7200_INTB_ClearInterrupt();    
    // Reset Ripple Counter
    RC_MRC_Write( LOW );
    // Save TIC value
    for( uint8_t stop_index = 1; stop_index <= gControlRegisters.TDC7200_num_stops; stop_index++ )
    {
        if ( stop_index == 1 )
            gStatusRegisters.TDC7200_timeInterval_1 = TDC7200_ReadMeasurement( 1 );
        else if ( stop_index == 2 )
            gStatusRegisters.TDC7200_timeInterval_2 = TDC7200_ReadMeasurement( 2 );
        else if ( stop_index == 3 )
            gStatusRegisters.TDC7200_timeInterval_3 = TDC7200_ReadMeasurement( 3 );
        else if ( stop_index == 4 )
            gStatusRegisters.TDC7200_timeInterval_3 = TDC7200_ReadMeasurement( 4 );
        else if ( stop_index == 5 )
            gStatusRegisters.TDC7200_timeInterval_3 = TDC7200_ReadMeasurement( 4 );
    }

}


void TIC_SetInputClockPath( TIC_INPUT_CLOCK tic_input_clock )
{
    RC_CLK_IN_SEL_Write( tic_input_clock );
}


void TIC_SetTdcInputPath( TDC_INPUT_PATH tdc_input_path )
{
    RC_CLK_OUT_SEL_Write( tdc_input_path );   
}

/* [] END OF FILE */
