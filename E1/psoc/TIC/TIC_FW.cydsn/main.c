/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <stdio.h>
#include "project.h"

#include "spi.h"
#include "adc.h"
#include "dma.h"
#include "tdc7200.h"
#include "tic.h"
#include "data_handler.h"
#include "ui.h"
#if defined(RUN_IN_DEVELOPMENT_MODE)
    #include "uart.h"    
#endif


uint8_t _status;
float32 _clock_frequency;


int main(void)
{        
    // Enable global interrupts
    CyGlobalIntEnable;
    
    #if defined(RUN_IN_DEVELOPMENT_MODE)
        UART_Init();                // UART
    #endif

    // Initialize PSoC Components
    SPI_Init();                 // SPI
    ADC_Init();                 // ADC
    DMA_Init();                 // DMA
    I2C_Init();                 // I2C

    //
    DATA_HANDLER_Init();
    UI_Init();

    // Initialize TIC Components
    TIC_Init();
    //
    TIC_SetInputClockPath( DUT_CLOCK );
    TIC_SetTdcInputPath( CLOCK_DIV2 );
    
    while ( 1 )
    {  
        UI_Run();
    }
    
}

/* [] END OF FILE */
