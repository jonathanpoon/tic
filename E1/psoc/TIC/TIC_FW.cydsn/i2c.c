/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "i2c.h"

// Variables
uint8_t _I2CS_read_buffer[I2CS_BUFFER_SIZE];
uint8_t _I2CS_write_buffer[I2CS_BUFFER_SIZE];
I2CS_BUFFER_STATUS _I2CS_read_buffer_status = I2CS_READ_BUFFER_FREE;


void I2C_Init( void )
{
    // Set I2CS Slave Address
    I2CS_SlaveSetAddress( 0x08 );
    // Clear I2C Read and Write Buffers 
    I2CS_ResetReadBuffer( I2CS_BUFFER_SIZE );
    I2CS_ResetWriteBuffer( I2CS_BUFFER_SIZE );
    // Initialize I2C Read and Write Buffers 
    I2CS_SlaveInitReadBuf( _I2CS_read_buffer, I2CS_BUFFER_SIZE );
    I2CS_SlaveInitWriteBuf( _I2CS_write_buffer, I2CS_BUFFER_SIZE );
    // Start I2CS
    I2CS_Start();
}

    
void I2CS_ResetReadBuffer( uint8_t buffer_length )
{
    memset( _I2CS_read_buffer, '\0', buffer_length );
}


void I2CS_ResetWriteBuffer( uint8_t buffer_length )
{
    memset( _I2CS_write_buffer, '\0', buffer_length );
}


I2CS_BUFFER_STATUS I2CS_GetReadBufferStatus( void )
{
    return _I2CS_read_buffer_status;
}


void I2CS_SetReadBufferStatus( I2CS_BUFFER_STATUS status )
{
    _I2CS_read_buffer_status = status;
}


void I2CS_SetReadBuffer( uint8_t* tmp_buffer, uint8_t tmp_buffer_length )
{
    memcpy( _I2CS_read_buffer, tmp_buffer, tmp_buffer_length );
}


void I2CS_SetReadBufferValue( uint8_t value, uint8_t index )
{
    _I2CS_read_buffer[index] = value;
}

/* [] END OF FILE */
