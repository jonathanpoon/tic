/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "LMT.h"

/********************************************************************
* Function Name: Acquire the ADC samples and save data in array
*********************************************************************/
float32 LMT70_VoltageToTemperature( float32 voltage )
{
    // Convert to mV
    voltage = voltage * 1E3;
    // Return temperature in Celsius
    return LMT70_A * pow(voltage, 3) + LMT70_B * pow(voltage, 2) + LMT70_C * voltage + LMT70_D;
}


float32 LMT87_VoltageToTemperature( float32 voltage )
{
    // Convert to mV
    voltage = voltage * 1E3;
    // Return temperature in Celsius
    return 1;
}

/* [] END OF FILE */
