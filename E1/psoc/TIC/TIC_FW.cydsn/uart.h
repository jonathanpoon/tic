/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef UART_H
#define UART_H

#include "project.h"

#include <stdlib.h>
#include <stdio.h>
#include "project.h"
#include "global_defs.h"

    
// Define macros
#define UART_RX_BUFFER_SIZE                         (16u)
#define UART_TX_BUFFER_SIZE                         (64u)
#define ST_REG_UARTM_TX_BITMASK                     0b01
    

// Global variables
extern uint8_t _UARTM_tx_buffer[UART_TX_BUFFER_SIZE];

#if defined(RUN_IN_DEVELOPMENT_MODE)
    // Function Defines
    void UART_Init( void );
    void UART_DisplayFrequency( float64 value ); 
    // void UART_ProcessInterrupt( void );
    // void UART_DisplayInputBuffer( void );
#endif

#endif    
    
/* [] END OF FILE */
