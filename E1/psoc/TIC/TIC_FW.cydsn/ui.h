/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef UI_H
#define UI_H

#include "project.h"
#include "i2c.h"
#include "data_handler.h"
#include "adc.h"
#include "tic.h"
    
#if defined(RUN_IN_DEVELOPMENT_MODE)
    #include "uart.h"    
#endif

// #define RUN_IN_DEVELOPMENT_MODE                     1
#define UI_VALID_COMMAND                            0x01

// Starting Register Address Location
#define CONTROL_REG_START_ADDR 		                0x40U   // 32 slots
#define STATUS_REG_START_ADDR 	    	            0x60U   // 32 slots
#define COMMAND_REG_START_ADDR    		            0x80U   // 32 slots

/********************************* COMMON section BEGIN 0x00 - 0x1F *******************************************/
typedef enum
{
    CMD_MEASURE_TIME_INTERVAL = 0x00,
    CMD_MEASURE_VOLTAGE = 0x01,
    CMD_OPTIONS_SIZE = 0x02,
} UI_CommandOptions;


typedef struct
{
    uint8_t writeShadow:1;
    uint8_t readShadow:1;
    uint8_t executeCommand:1;
    uint8_t controlRegisterAccess:1;
    uint8_t statusRegisterAccess:1;
} UI_StatusFlag_t;

extern UI_StatusFlag_t _UI_StatusFlags;

    
typedef struct {
	uint8_t i2cRegAddress;                      // 8 bit reg. Indicates which variable to read or write.
    uint8_t i2cWriteDataLength;                 // [byte], length of effective data in rxDataBuffer
    uint8_t i2cReadDataLength;                  // [byte], length of effective data in rxDataBuffer
    
    uint8_t regIndex;
    uint8_t* regDataPtr;                        // pointer to data to tx
    uint8_t regDataLength;                      // [byte], length of effective data in rxDataBuffer                                  
    
    UI_CommandOptions commandOption;
    uint8_t	commandDataBuffer[I2CS_DATA_SIZE];   // data buffer for SPI reponse or other functions
    uint8_t	commandDataLength;                  // data buffer for SPI reponse or other functions
    
    uint8_t streamingEnabled:1;
} UI_Control_t;
    
extern UI_Control_t _UI_Control;


// Function Defines
void UI_Init( void );
void UI_Run( void );
uint8_t UI_ParseWriteRequest( void );
void UI_ProcessWriteRequest( void );
uint8_t UI_ExecuteCommand( void );
void UI_UpdateI2CReadBuffer( uint8_t* dataBuffer, uint8_t dataLength );
void UI_ResetStatusFlags( void );

#endif

/* [] END OF FILE */
