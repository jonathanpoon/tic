/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef ADC_H
#define ADC_H

#include "project.h"
#include "dma.h"

// Defines
#define ADC_DS_BUFFER_SIZE                          (16u)

// Global Variables
extern uint16_t _ADC_DS_buffer[ADC_DS_BUFFER_SIZE];
extern uint16_t _ADC_DS_voltage;

// Define Functions
void ADC_Init( void );
void ADC_DS_GetSamples( void );
void ADC_DS_GetMeanVoltage( void );

#endif

/* [] END OF FILE */
