/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef ISR_H
#define ISR_H

#include "project.h"
#include "uart.h"

// Function Defines
void isr_Init( void );
// CY_ISR_PROTO( isr_UART );

#endif

/* [] END OF FILE */
