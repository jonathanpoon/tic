/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/


#include "tdc7200.h"


uint8_t _TDC7200_spi_tx_buffer[TDC7200_SPI_BUFFER_SIZE];
uint8_t _TDC7200_spi_rx_buffer[TDC7200_SPI_BUFFER_SIZE];

// Global variables
uint64_t _TDC7200_clkPeriodPs;     //< Clock period in [ps].
uint8_t _TDC7200_cal2Periods;     //< Calibration2, number of measuring clock periods, one of [2,10,20,40].
uint8_t _TDC7200_config1;         //< CONFIG1 register value, used to start measurement.
uint8_t _TDC7200_mode;            //< Measurement mode [1,2].
uint8_t _TDC7200_numStops;        //< Number of stops per measurement.
int64_t _TDC7200_normLsb;         //< Cached normLsb value for tof calculation.
uint64_t _TDC7200_overflowPs;      //< Overflow time, in [ps].
uint8_t _TDC7200_reg_value, _TDC7200_status_flag;
uint64 _TDC7200_tof;


uint8_t TDC7200_Init()
{
    // Initialize Variables
    _TDC7200_clkPeriodPs = PS_PER_SEC / TDC7200_REF_CLK_FREQ_HZ;
    _TDC7200_overflowPs = 0u;
    
    // -- Enable TDC7200 - Reset to default configuration
    TDC7200_EN_Write( LOW );
    CyDelay( TDC7200_ENABLE_LOW_MS );   // Disable for a short time
    
    // Enable and wait the maximum time after enabling LDO to assure VREG is stable.
    TDC7200_EN_Write( HIGH );
    CyDelay( TDC7200_ENABLE_T3_LDO_SET3_MS );

    // -- Verify SPI Read communication with TDC7200
    if ( TDC7200_ReadReg8( TDC7200_REG_ADR_CONFIG2 ) != TDC7200_REG_DEFAULTS_CONFIG2 )  { return 0; }
    if ( TDC7200_ReadReg8( TDC7200_REG_ADR_INT_MASK ) != TDC7200_REG_DEFAULTS_INT_MASK )    { return 0; }

    // Assert interrupt output on overflow and measurement finished
    TDC7200_WriteReg8(TDC7200_REG_ADR_INT_MASK, BIT(TDC7200_REG_SHIFT_INT_MASK_CLOCK_CNTR_OVF_MASK)
                                           | BIT(TDC7200_REG_SHIFT_INT_MASK_COARSE_CNTR_OVF_MASK)
                                           | BIT(TDC7200_REG_SHIFT_INT_MASK_NEW_MEAS_MASK) );    
    
    //
    return 1u;
}

uint8_t TDC7200_SetupMeasurement( const uint8_t cal2Periods, const uint8_t avgCycles, const uint8_t mode, const uint8_t numStops )
{
    uint8_t config2 = 0u;

    // Config2 Calibration2 periods
    if      (cal2Periods == 2)  config2 = TDC7200_REG_VAL_CONFIG2_CALIBRATION2_PERIODS_2  << TDC7200_REG_SHIFT_CONFIG2_CALIBRATION2_PERIODS;
    else if (cal2Periods == 10) config2 = TDC7200_REG_VAL_CONFIG2_CALIBRATION2_PERIODS_10 << TDC7200_REG_SHIFT_CONFIG2_CALIBRATION2_PERIODS;
    else if (cal2Periods == 20) config2 = TDC7200_REG_VAL_CONFIG2_CALIBRATION2_PERIODS_20 << TDC7200_REG_SHIFT_CONFIG2_CALIBRATION2_PERIODS;
    else if (cal2Periods == 40) config2 = TDC7200_REG_VAL_CONFIG2_CALIBRATION2_PERIODS_40 << TDC7200_REG_SHIFT_CONFIG2_CALIBRATION2_PERIODS;
    else return 0;
    _TDC7200_cal2Periods = cal2Periods;

    // Config2 Avg Cycles
    uint8_t val = TDC7200_REG_VAL_CONFIG2_AVG_CYCLES_MIN_VAL;
    do {
        if ( (1 << val) == avgCycles )
        {
            config2 |= val << TDC7200_REG_SHIFT_CONFIG2_AVG_CYCLES;
            break;
        }
        ++val;
    } while ( val <= TDC7200_REG_VAL_CONFIG2_AVG_CYCLES_MAX_VAL );
    if ( val > TDC7200_REG_VAL_CONFIG2_AVG_CYCLES_MAX_VAL )
        return 0;

    // Config2 Num Stops
    if ( numStops == 0 || numStops > TDC7200_REG_VAL_CONFIG2_NUM_STOP_MAX ) {   return 0u;   }
    _TDC7200_numStops = numStops;
    config2 |= TDC7200_REG_VAL_CONFIG2_NUM_STOP(numStops) << TDC7200_REG_SHIFT_CONFIG2_NUM_STOP;
    
    // Config1 Mode
    if ( mode < TDC7200_REG_VAL_CONFIG1_MEAS_MODE_MIN || mode > TDC7200_REG_VAL_CONFIG1_MEAS_MODE_MAX ) {   return 0u;  }
    _TDC7200_mode = mode;
    _TDC7200_config1 = TDC7200_REG_VAL_CONFIG1_MEAS_MODE(mode) << TDC7200_REG_SHIFT_CONFIG1_MEAS_MODE;

    // Mode influences overflow, so update now.
    TDC7200_SetupOverflow( _TDC7200_overflowPs );

    // Set STOP pulse as falling edge
    // _TDC7200_config1 |= BIT( TDC7200_REG_SHIFT_CONFIG1_STOP_EDGE );
    // Set TRIGGER pulse as falling edge
    _TDC7200_config1 |= BIT( TDC7200_REG_SHIFT_CONFIG1_TRIGG_EDGE );    
    // Config1 Start measurement
    _TDC7200_config1 |= BIT( TDC7200_REG_SHIFT_CONFIG1_START_MEAS );

    // Write CONFIG2 Register
    TDC7200_WriteReg8( TDC7200_REG_ADR_CONFIG2, config2 );

    // Return
    return 1u;
}


/**
    * Setup stop mask.
    * @param[in] stopMaskPs  Duration of stop mask, in [ps]. Will be rounded to number of external
    *                        clock counts, so actual value used might be slightly different.
*/

void TDC7200_SetupStopMask( const uint64_t stopMaskPs )
{
    // Convert duration of stopmask from [ps] to clock increments.
    uint16_t stopMaskClk = stopMaskPs / (uint64_t)PS_PER_SEC / (uint64_t)TDC7200_REF_CLK_FREQ_HZ;
    TDC7200_WriteReg8( TDC7200_REG_ADR_CLOCK_CNTR_STOP_MASK_H, stopMaskClk >> 8 );
    TDC7200_WriteReg8( TDC7200_REG_ADR_CLOCK_CNTR_STOP_MASK_L, stopMaskClk );
}


/**
    * Setup overflow time.
    * @param[in] overflowPs  Overflow time, in [ps]. If tof takes longer than overflowPs,
    *                        a timeout will occur.
    * @remark Currently only functional for mode2, as formula for mode1 doesn't seem correct.
*/

uint8_t TDC7200_SetupOverflow( const uint64_t overflowPs )
{
    // Datasheet is rather vague on overflow configuration...
    // Some info from the net:
    //
    // https://e2e.ti.com/support/sensor/ultrasonic/f/991/p/445361/1905773?tisearch=e2e-sitesearch&keymatch=translation&pi239031350=2
    // For 8MHz clock:
    // In that case it depends on what's selected in the overflow registers (CLOCK_CNTR_OV_H/L registers
    // for mode 2, COARSE_CNTR_OV_H/L registers for mode 1). By default, they are set to 0xFFFF, which is
    // the maximum time out value. In mode 2, this corresponds to 8.192ms. In mode 1, the maximum time
    // out period corresponds to 454.164us.
    // --
    // For mode 1, all the counting is done using the internal time base (ring oscillator) which runs at 144MHz
    // (6.93ns). This translates to a timeout period of roughly 454.16us. Note you don't want to run Mode 1 for
    // long periods (> 500ns) as explained in Section 8.4.2.1 of the DS.
    //
    // https://e2e.ti.com/support/sensor/ultrasonic/f/991/p/475519/1710858
    // if I want to set a 300ns overflow in mode 1, how do I calculate the value should be written to COARSE_CNTR_OVF
    // You should use
    // COARSE_CNTR_OVF = 300ns / (normLSB * 63).
    // where 63 is the number of delay cells in the internal ring oscillator.
    // You can use the typical normLSB value of 55ps but if you want higher accuracy, then you can measure
    // normLSB (once every minute or so as your application permits) and update the COARSE_CNTR_OVF register.

    uint16_t coarseOvf = 0xFFFFu;   // For mode 1, maximum 454.164us
    uint16_t clockOvf  = 0xFFFFu;   // For mode 2, maximum 8.192ms

    // setupOverflow of 0 means as long as possible, so use to max. value.
    if (overflowPs)
    {
        if ( _TDC7200_mode == 1 )
        {
            // Calculate number of ring oscillator ticks
            // Note: (TDC7200_RING_OSC_FREQ_HZ / PS_PER_SEC) == (TDC7200_RING_OSC_FREQ_MHZ / US_PER_SEC)
            const uint32_t ovf = ( overflowPs * TDC7200_RING_OSC_FREQ_MHZ ) / US_PER_SEC;
            // Clip to upper bound.
            if (ovf < 0xFFFFu)
            {
                coarseOvf = ovf;
            }
        }
        else
        {
            const uint32_t ovf = overflowPs / _TDC7200_clkPeriodPs;
            // Clip to upper bound.
            if (ovf < 0xFFFFu)
            {
                clockOvf = ovf;
            }
        }
    }
    
    // Write both overflow for mode 1 & 2. If mode 1 is active, mode 2 will be set to max and vice versa.
    TDC7200_WriteReg8( TDC7200_REG_ADR_COARSE_CNTR_OVF_H, coarseOvf >> 8 );
    TDC7200_WriteReg8( TDC7200_REG_ADR_COARSE_CNTR_OVF_L, coarseOvf );
    TDC7200_WriteReg8( TDC7200_REG_ADR_CLOCK_CNTR_OVF_H, clockOvf >> 8 );
    TDC7200_WriteReg8( TDC7200_REG_ADR_CLOCK_CNTR_OVF_L, clockOvf );

    // Remember for mode changes
    _TDC7200_overflowPs = overflowPs;
    
    return 1;
}


/**
    * Start a new measurement.
*/

void TDC7200_StartMeasurement( )
{
    // Clear status
    TDC7200_WriteReg8( TDC7200_REG_ADR_INT_STATUS,
        ( BIT(TDC7200_REG_SHIFT_INT_STATUS_MEAS_COMPLETE_FLAG) |
           BIT(TDC7200_REG_SHIFT_INT_STATUS_MEAS_STARTED_FLAG) |
           BIT(TDC7200_REG_SHIFT_INT_STATUS_CLOCK_CNTR_OVF_INT) |
           BIT(TDC7200_REG_SHIFT_INT_STATUS_COARSE_CNTR_OVF_INT) |
           BIT(TDC7200_REG_SHIFT_INT_STATUS_NEW_MEAS_INT)
        )
    );
    
    // Force recalculation of normLsb after measurement ended
    _TDC7200_normLsb = 0ull;

    // Start measurement
    TDC7200_WriteReg8( TDC7200_REG_ADR_CONFIG1, _TDC7200_config1 );
    
    // CyDelayUs( 500 );
}



/**
    * Read measurement result.
    * Caller must make sure sufficient time has elapsed for all pulses to occur before calling this function.
    * @param[in]  stop       Index of stop pulse to read time for, [1..numStops].
    * @param[out] tof        Measured time-of-flight from start pulse to given stop pulse, or 0 when
    *                        pulse wasn't recorded (didn't occur, or not within overflow time).
    * @return                True, when all parameters were valid and time-of-flight was retrieved.
*/

uint64_t TDC7200_ReadMeasurement( const uint8_t stop_index )
{
    _TDC7200_tof = 0u;

    if ( stop_index > _TDC7200_numStops )
        return 0u;
    
    // Verify the measurement is complete
//    _TDC7200_reg_value = TDC7200_ReadReg8( TDC7200_REG_ADR_INT_STATUS );
//    if ( !(_TDC7200_reg_value & BIT(TDC7200_REG_SHIFT_INT_STATUS_MEAS_COMPLETE_FLAG)) )  {   return 0u;   }
    //if ( !(TDC7200_ReadReg8( TDC7200_REG_ADR_INT_STATUS ) & BIT(TDC7200_REG_SHIFT_INT_STATUS_MEAS_COMPLETE_FLAG)) )  {   return 0u;   }

    // multiplier (2^shift) used to prevent rounding errors
    const uint8_t shift = 20;

    // Speed optimize: Cache normLsb for multiple stop tof calculations
    if ( !_TDC7200_normLsb )
    {
        const uint32_t calibration1 = TDC7200_ReadReg24( TDC7200_REG_ADR_CALIBRATION1 );
        const uint32_t calibration2 = TDC7200_ReadReg24( TDC7200_REG_ADR_CALIBRATION2 );
        // calCount scaled by 2^shift
        const int64_t calCount = ( ( (int64_t)calibration2 - (int64_t)calibration1 ) << shift ) / (int64_t)(_TDC7200_cal2Periods - 1);

        // normLsb scaled by 2^shift, divided by calcount (scaled by 2^shift),
        // so multiply by 2^(2*shift) to compensate for divider in calCount
        _TDC7200_normLsb  = ( (uint64_t)_TDC7200_clkPeriodPs << (2 * shift) ) / calCount;
    }

    switch ( _TDC7200_mode )
    {
        case 1:
        {
            const uint32_t timen = TDC7200_ReadReg24( TDC7200_REG_ADR_TIMEX(stop_index) );          // TIME(n)
            _TDC7200_tof = ( (int64_t)timen * _TDC7200_normLsb ) >> shift;
            break;
        }
        case 2:
        {
            const uint32_t time1 = TDC7200_ReadReg24( TDC7200_REG_ADR_TIME1 );                // TIME1
            const uint32_t timen1 = TDC7200_ReadReg24( TDC7200_REG_ADR_TIMEX(stop_index + 1) );      // TIME(n+1)
            const uint32_t clockCountn = TDC7200_ReadReg24( TDC7200_REG_ADR_CLOCK_COUNTX(stop_index) );   // CLOCK_COUNT(n)
            _TDC7200_tof = ( ( (int64_t)time1 - (int64_t)timen1 ) * _TDC7200_normLsb ) >> shift;
            _TDC7200_tof += (uint64_t)clockCountn * (uint64_t)_TDC7200_clkPeriodPs;
            break;
        }
        default: return 0ull;
    }
    // TOF for a pulses that didn't occur will be reported as all ones.
    // If this is the case, return 0 as value.
    if ( !(~_TDC7200_tof) )  { _TDC7200_tof = 0ull; }
    
    return _TDC7200_tof;
}



void TDC7200_SpiTest( void )
{    
    _TDC7200_reg_value = TDC7200_ReadReg8( TDC7200_REG_ADR_CONFIG1 );
    _TDC7200_reg_value = TDC7200_ReadReg8( TDC7200_REG_ADR_CONFIG2 );
    _TDC7200_reg_value = TDC7200_ReadReg8( TDC7200_REG_ADR_INT_STATUS );
    _TDC7200_reg_value = TDC7200_ReadReg8( TDC7200_REG_ADR_COARSE_CNTR_OVF_H );
    _TDC7200_reg_value = TDC7200_ReadReg8( TDC7200_REG_ADR_COARSE_CNTR_OVF_L );
    _TDC7200_reg_value = TDC7200_ReadReg8( TDC7200_REG_ADR_CLOCK_CNTR_OVF_H );
    _TDC7200_reg_value = TDC7200_ReadReg8( TDC7200_REG_ADR_CLOCK_CNTR_OVF_L );
    _TDC7200_reg_value = TDC7200_ReadReg8( TDC7200_REG_ADR_CLOCK_CNTR_OVF_L );
    _TDC7200_reg_value = TDC7200_ReadReg8( TDC7200_REG_ADR_CLOCK_CNTR_STOP_MASK_H );
    _TDC7200_reg_value = TDC7200_ReadReg8( TDC7200_REG_ADR_CLOCK_CNTR_STOP_MASK_L );
    // TDC7200_WriteReg8( TDC7200_REG_ADR_CONFIG2, 96 );
    // TDC7200_WriteReg8( TDC7200_REG_ADR_CONFIG2, 80 );
}


uint8_t TDC7200_ReadReg8( const uint8_t reg_addr )
{
    //
    memset( _TDC7200_spi_tx_buffer, 0, TDC7200_SPI_BUFFER_SIZE );
    memset( _TDC7200_spi_rx_buffer, 0, TDC7200_SPI_BUFFER_SIZE );
    
    _TDC7200_spi_tx_buffer[0] = ( reg_addr & TDC7200_SPI_REG_ADDR_MASK ) | TDC7200_SPI_REG_READ;
    SPI_TDC7200_TransmitReceive16();
    
    // Return 8-bit value
    return _TDC7200_spi_rx_buffer[1];
}


uint32_t TDC7200_ReadReg24( const uint8_t reg_addr )
{
    memset( _TDC7200_spi_tx_buffer, 0, TDC7200_SPI_BUFFER_SIZE );
    memset( _TDC7200_spi_rx_buffer, 0, TDC7200_SPI_BUFFER_SIZE );
    
    _TDC7200_spi_tx_buffer[0] = ( reg_addr & TDC7200_SPI_REG_ADDR_MASK ) | TDC7200_SPI_REG_READ;
    SPI_TDC7200_TransmitReceive32();
    
    /*
    uint32_t reg_value = 0u;
    reg_value = _TDC7200_spi_rx_buffer[1];
    reg_value <<= 8;
    reg_value |= _TDC7200_spi_rx_buffer[2];
    reg_value <<= 8;
    reg_value |= _TDC7200_spi_rx_buffer[3];
    */
    
    return ( _TDC7200_spi_rx_buffer[1] << 16 ) + ( _TDC7200_spi_rx_buffer[2] << 8 ) + _TDC7200_spi_rx_buffer[3];
}


uint8_t TDC7200_WriteReg8( const uint8_t reg_addr, const uint8_t reg_value )
{
    memset( _TDC7200_spi_tx_buffer, 0, TDC7200_SPI_BUFFER_SIZE );
    memset( _TDC7200_spi_rx_buffer, 0, TDC7200_SPI_BUFFER_SIZE );
    _TDC7200_spi_tx_buffer[0] = ( reg_addr & TDC7200_SPI_REG_ADDR_MASK ) | TDC7200_SPI_REG_WRITE;
    _TDC7200_spi_tx_buffer[1] = reg_value;
    SPI_TDC7200_TransmitReceive16();
    CyDelayUs( 5 );
    
    // _TDC7200_reg_value = TDC7200_ReadReg8( reg_addr );
    // if ( _TDC7200_reg_value != reg_value )  {   return 0u;  }
    return 1u;
}

/* [] END OF FILE */
