/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef TIC_H
#define TIC_H

#include "project.h"
#include "tdc7200.h"

    
/* tic.h Defines */
#define TDC7200_DEFAULT_CAL_PERIODS                         (10u)
#define TDC7200_DEFAULT_AVG_CYCLES                          (1u)
#define TDC7200_DEFAULT_NUM_STOPS                           (1u)
#define TDC7200_DEFAULT_MODE                                (2u)    
#define TIC_DEFAULT_CLK_DIVIDER                             (65536u)

typedef enum
{
    EXT_CLOCK = 0,
    DUT_CLOCK = 1
} TIC_INPUT_CLOCK;

typedef enum
{
    CLOCK_DIV1 = 0,
    CLOCK_DIV2 = 1
} TDC_INPUT_PATH;

extern double _TIC_time_interval_measurement[TDC7200_REG_VAL_CONFIG2_NUM_STOP_MAX];

    
// Function Defines
void TIC_Init( void );
void TIC_MeasureTimeInterval( void );
float32 TIC_CalcFrequency( uint32_t clockDivider, float32 psTimeOffset );
void TIC_SetInputClockPath( TIC_INPUT_CLOCK tic_input_clock );
void TIC_SetTdcInputPath( TDC_INPUT_PATH tdc_input_path );

#endif

/* [] END OF FILE */
